# report-user.sh

Quick shell script to report various things about a user.  Must be run as root.

Reports the status of a user's password and ssh key files.

See the script for additional details.

