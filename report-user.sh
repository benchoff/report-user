#!/bin/bash
#
# Reports the status of a user's password and
# sha256 sums for SSH authorized keys files.
#
# Takes one argument: the username
#

# These are the files that are checked.
ssh_authorized_key_files=".ssh/authorized_keys .ssh/authorized_keys_2"

# returns sha256sum for a file or 0 if it does not exist
check_file() {
  fn="$1"
  if [ -f "$fn" ] ; then
    /bin/sha256sum "$fn" | cut -d' ' -f1
  else
    echo "0"
  fi
}

# echo report for a single user
# fields:
#   - hostname
#   - username
#   - exit code from passwd --status
#   - password status, or UK for unknown user, or stderr from passwd
#  if the passwd command worked:
#   - list of <filename>=<sha256sum>
#
check_user() {
  user="$1"
  pws="$(/bin/passwd --status "$user" 2>&1)"
  pwrc="$?"
  echo -n "$(/bin/hostname) $user"
  echo -n " $pwrc"
  if [ "$pwrc" -eq 0 ] ; then
    # user exists.  The second field is the password status.
    echo -n " $(echo "$pws" | /bin/cut -d' ' -f2)"
    uhome="$(eval "echo "~${user}"")"
    for f in $ssh_authorized_key_files ; do
      kfn="${uhome}/$f"
      echo -n " $kfn=$(check_file "$kfn")"
    done
  elif [ "$pwrc" -eq 252 ] ; then
    # user does not exist
    echo -n " UK"
  else
    # unknown error
    echo -n " $pws"
  fi
  echo ""

}

if [ "$#" -lt 1 ] ; then
  echo "This command takes arguments: list of users to test." >&2
  exit 1
fi

users="$*"
for user in $users ; do
  check_user "$user"
done
